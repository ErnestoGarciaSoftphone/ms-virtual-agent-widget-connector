# Microsoft Power Virtual Agent Connector for Genesys (client-side)

This is an extension to the open-source [Microsoft Web Chat](https://github.com/microsoft/BotFramework-WebChat) widget. It allows to start a conversation with a DirectLine bot (like a Microsoft [Power Virtual Agent](https://powervirtualagents.microsoft.com/)), and switch to a Genesys chat conversation.

## This is a prototype!

The purpose of this prototype has been to:

- [x] Find a way to plug a custom chat implementation to the [Microsoft Web Chat](https://github.com/microsoft/BotFramework-WebChat) widget. (Basically implementing [IBotConnection](https://github.com/microsoft/BotFramework-DirectLineJS/blob/8df1caa62be67a51e0b8804eec9292eb44ad7026/src/directLine.ts#L444)).

- [x] Try to fake the continuation of a conversation after a [handoff](https://docs.microsoft.com/en-us/azure/bot-service/bot-service-design-pattern-handoff-human).

- [x] Build an adapter for Genesys chat.

- [x] Check if we can build the Genesys chat adapter by making use of the [Widget v2](https://developer.mypurecloud.com/api/webchat/widget-version2.html) [WebChatService](https://docs.genesys.com/Documentation/GWC/latest/WidgetsAPI/WebChatService). This would allow using the same adapter for both Genesys Cloud and Genesys Engage. (Of course, the more specific client APIs for each of the platforms could also be used, and might be preferrable for more control).

- [x] Prove the whole scenario of handoff a conversation from a Power Virtual Agent to a Genesys Cloud agent.

- [x] Prove that the transcript of the previous chat with the bot can be sent to the Genesys Cloud Agent.

This prototype also contains an example on how to show the Microsoft Web Chat as an overlay widget.

## Deployment

### Power Virtual Agent

Publish your own Power Virtual Agent. Create a topic that will handoff the conversation to an agent, by adding a node of type "End the conversation > Transfer to agent".

Go to "Manage > Channels > Mobile app" to obtain Bot ID and Tenant ID of your virtual agent.

Configure those in index.html, as query parameters of the powerva.microsoft.com URL.

### Genesys chat

Configure your Genesys chat parameters in index.html. For Genesys Cloud, that includes your base URL, org id, deployment id, and chat queue name.

### Run the prototype

Open a terminal and run:

    > parcel index.html

This will start a local server on port 1234.

### Run the scenario

Open a browser and navigate to http://localhost:1234. This should open index.html, and show the chat widget.

Engage in a conversation with the bot. Say something to trigger the handoff to an agent logic. That should create a new Genesys Cloud chat interaction.

Start a Genesys Cloud agent that belongs to the configured chat queue above. Put the agent to status "On Queue". The agent should receive the chat interaction, and any past chat transcript in the first incoming message.

## Pending

Take action when a page is refreshed or navigated away: it should continue with either the Virtual Agent conversation, or with the real Genesys agent. Check before starting the conversation. The Genesys conversation will not correctly start if there was an ongoing conversation before.

Fully implement chat lifecycle, including ending chat interactions.

Implement all interesting chat activities, including 'typing' and 'joined' activities.