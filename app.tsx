// import * as ReactDOM from 'react-dom';import * as React from 'react';
// ReactDOM.render(React.createElement("h2", null, "Hello, world!"), document.body);

import { IBotConnection, Activity, ConnectionStatus, Message, EventActivity, IActivity } from 'botframework-directlinejs';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs';

export class MyBotConnection implements IBotConnection {
  public connectionStatus$ = new BehaviorSubject(ConnectionStatus.Uninitialized);
  public activity$: Observable<Activity>;
  end(): void {
    console.log("MyBotConnection.end() called");
  }
  postActivity(activity: Activity): Observable<string> {
    console.log("MyBotConnection.postActivity() called", activity);
    return Observable.from("testing");
  }
  getSessionId?: () => Observable<string>;
}

export class BotLogger implements IBotConnection {
  connectionStatus$: BehaviorSubject<ConnectionStatus>;
  activity$: Observable<Activity>;

  readonly botConnection: IBotConnection;
  
  constructor(botConnection: IBotConnection) {
    this.botConnection = botConnection;
    botConnection.connectionStatus$.subscribe(s => {
      console.log("connectionStatus changed", s);
      this.connectionStatus$ = botConnection.connectionStatus$;
    });

    this.activity$ = botConnection.activity$.do(a => 
      console.log("activity$ received", a));
  }
  
  end(): void {
    console.log("MyBotConnection.end() called");
    this.botConnection.end();
  }

  postActivity(activity: Activity): Observable<string> {
    console.log("MyBotConnection.postActivity() called", activity);
    return this.botConnection.postActivity(activity).map(s => {
      console.log("postActivity result received", s);
      return s;
    });
  }

  getSessionId(): Observable<string> {
    return this.botConnection.getSessionId().map(s => {
      console.log("response to getSessionId received", s);
      return s;
    });
  }
}

function delay(ms: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms)
  });
}

export class MyFakeBot implements IBotConnection {
  public connectionStatus$ = new BehaviorSubject(ConnectionStatus.Uninitialized);
  public activity$: Subject<Activity> = new Subject<Activity>();

  private conversationId: string;
  private activityIdNumber: number;

  constructor(activity: Activity) {
    console.log("Fake bot created");
    this.conversationId = activity.conversation.id;
    this.activityIdNumber = parseInt(activity.id.split('|')[1]) + 1;
    this.initConnectionStatus();
  }

  async initConnectionStatus() {
    await delay(500);
    this.connectionStatus$.next(ConnectionStatus.Connecting);    
    await delay(500);
    this.connectionStatus$.next(ConnectionStatus.Online);
  }

  end(): void {
  }

  nextActivityId(): string {
    return this.conversationId + '|' + (this.activityIdNumber++).toString().padStart(7, "0");
  }

  postActivity(activity: Activity): Observable<string> {
    let enrichedActivity = {
      ...activity,
      id: this.nextActivityId(),
      channelId: 'directline',
      conversation: {id: this.conversationId},
      // recipient:
        // id: "9085d117-764a-4038-afb5-5b6a4513e0e9@L7RLmja9fb0"
        // name: "My bot sample"      
    };

    let replyActivityTemplate = {
      ...activity,
      id: this.nextActivityId(),
      channelId: 'directline',
      conversation: {id: this.conversationId},
      from: {role: 'bot' as const, id: 'BOT_ID', name: "Fake bot"},
      replyToId: enrichedActivity.id,
      timestamp: new Date().toISOString(),
    };

    delete replyActivityTemplate.channelData;
    delete replyActivityTemplate.text;
    delete replyActivityTemplate.textFormat;
    delete replyActivityTemplate.type;

    let result = new Subject<string>();

    (async () => {
      await delay(500);
      this.activity$.next(enrichedActivity);
      result.next(enrichedActivity.id);
      result.complete();

      await delay(500);
      this.activity$.next({
        ...replyActivityTemplate,
        id: this.nextActivityId(),
        type: 'message',
        text: "this is the **bot's response**",
        textFormat: 'markdown' as const,
        timestamp: new Date().toISOString(),
      } as Message);

      await delay(500);
      this.activity$.next({
        ...replyActivityTemplate,
        id: this.nextActivityId(),
        type: 'message',
        text: "this is the **second** reponse",
        textFormat: 'markdown' as const,
        timestamp: new Date().toISOString(),
      } as Message);

      // await delay(500);
      // this.activity$.next({
      //   ...replyActivityTemplate,
      //   id: this.nextActivityId(),
      //   type: 'event',
      //   name: 'handoff.initiate',
      //   timestamp: new Date().toISOString(),
      // } as EventActivity);
    })();

    return result;
  }

  getSessionId?: () => Observable<string> = null;
}

export class HandoffBot implements IBotConnection {
  private readonly handoffBotFactory: IBotConnection;

  connectionStatus$: BehaviorSubject<ConnectionStatus>;
  activity$: Observable<Activity>;

  currentBot$: BehaviorSubject<IBotConnection>;

  constructor(initialBot: IBotConnection, handoffBotFactory: (EventActivity) => IBotConnection) {
    this.currentBot$ = new BehaviorSubject<IBotConnection>(initialBot);
    this.connectionStatus$ = initialBot.connectionStatus$;
    this.activity$ = this.currentBot$
      .map(bot => bot.activity$)
      .switch();
    
    initialBot.activity$.subscribe(activity => {
      if (activity.type == 'event' && activity.name == 'handoff.initiate') {
        console.log("Handoff detected!", activity);
        let handoffBot = handoffBotFactory(activity);
        this.currentBot$.next(handoffBot);
      }
    });
  }

  end(): void {
    this.currentBot$.value.end();
  }
  
  postActivity(activity: Activity): Observable<string> {
    return this.currentBot$.value.postActivity(activity);
  }
}

export class GenesysWidgetsBot implements IBotConnection {
  connectionStatus$: BehaviorSubject<ConnectionStatus> = new BehaviorSubject<ConnectionStatus>(ConnectionStatus.Online);
  activity$: Subject<Activity> = new Subject<Activity>();
  
  private readonly widgetsPlugin: any;
  private readonly conversationId: string;
  // private readonly replyActivityTemplate: IActivity;

  private nextActivityIdNumber: number;

  constructor(handoffActivity: EventActivity, widgetsBus) {
    this.conversationId = handoffActivity ? handoffActivity.conversation.id : "GENESYS_CONVERSATION";
    this.nextActivityIdNumber = handoffActivity ? parseInt(handoffActivity.id.split('|')[1]) + 1 : 0;
    // this.replyActivityTemplate = {
    //   ...handoffActivity,
    //   id: this.nextActivityId(),
    //   channelId: 'directline',
    //   conversation: {id: this.conversationId},
    //   from: {role: 'bot' as const, id: 'BOT_ID', name: "Fake bot"},
    //   // replyToId: enrichedActivity.id,
    //   timestamp: new Date().toISOString(),
    // };

    this.widgetsPlugin = widgetsBus.registerPlugin('Softphone.GenesysWidgetsBot');

    this.widgetsPlugin.subscribe('WebChatService.started', function (e) {
      console.log('Chat started', e);
    });

    this.widgetsPlugin.subscribe('WebChatService.ended', function (e) {
      console.log('Chat ended', e);
    });

    this.widgetsPlugin.subscribe('WebChatService.messageReceived', obj => {
      console.log('WebChatService.messageReceived', obj);

      for (let message of obj.data.messages) {
        if (message.type == 'Message' && message.from.type != 'Client') {
          this.activity$.next({
            id: this.nextActivityId(),
            channelId: 'directline',
            conversation: {id: this.conversationId},
            from: {role: 'bot' as const, id: 'BOT_ID', name: "Genesys agent"},
            timestamp: new Date().toISOString(),
            type: "message",
            text: message.text,
            textFormat: "markdown",
          } as Message);
        }
      }
    });

    this.widgetsPlugin.command('WebChatService.startChat', 
    {
      nickname: 'Jonny',
      firstname: 'Johnathan',
      lastname: 'Smith',
      email: 'jon.smith@mail.com',
      subject: 'product questions',
      userData: {},
    })
    .done(e => {
      console.log("WebChatService.startChat succeeded", e);

      let transcript = [];

      transcript.push("#### **_Previous conversation with bot:_**\n");

      if (handoffActivity.attachments) {
        const transcriptAttachment = handoffActivity.attachments.find(attachment => attachment.name == 'Transcript');
        if (transcriptAttachment) {
          for (let activity of transcriptAttachment.content.activities) {
            let isBot = activity.from.role == 'bot';

            function formatActivityIcon(isBot, activity) : string {
              return (isBot ? "\u{1F5EF} _**Bot**_ " : "\u{1F5E8} _**User**_ ")
                + activity.text 
                + "\n\n";
            }

            function formatActivityQuote(isBot, activity) : string {
              return (isBot ? "_**Bot**_ " : "> _**User**_ ")
                + activity.text 
                + "\n\n";
            }

            function formatActivityCode(isBot, activity) : string {
              return (isBot ? "```\n" : "_**User**_ ")
                + activity.text 
                + (isBot ? "\n```" : "")
                + "\n\n";
            }

            function formatActivityImage(isBot, activity) : string {
              return (isBot ? '![Bot](http://localhost:1234/chatbot.png "Bot")\n' : "_**User**_ ")
                + activity.text 
                + "\n\n";
            }

            // transcript.push(formatActivityIcon(isBot, activity));
            // transcript.push(formatActivityQuote(isBot, activity));
            transcript.push(formatActivityCode(isBot, activity));
            // transcript.push(formatActivityImage(isBot, activity));
          }
        }
      }

      this.widgetsPlugin.command('WebChatService.sendMessage', {message: transcript.join('')});
    })
    .fail(e => {
      console.error("WebChatService.startChat failed", e);
    });
  }

  end(): void {
    throw new Error("Method not implemented.");
  }

  postActivity(activity: Activity): Observable<string> {
    if (activity.type == 'message') {
      let message = activity as Message;
      let result = new Subject<string>();
      this.widgetsPlugin.command('WebChatService.sendMessage', {message: activity.text})
        .done(data => {
          // data.status == 'resolved'??
          // response == data
          let enrichedActivity = {
            ...activity,
            id: this.nextActivityId(),
            channelId: 'directline',
            conversation: {id: this.conversationId},
            // recipient:
              // id: "9085d117-764a-4038-afb5-5b6a4513e0e9@L7RLmja9fb0"
              // name: "My bot sample"      
          };      

          this.activity$.next(enrichedActivity);
          result.next(enrichedActivity.id);
          result.complete();
        })
        .fail(e => {
          result.error(e);
        });

      return result;
    }
  }

  nextActivityId(): string {
    return this.conversationId + '|' + (this.nextActivityIdNumber++).toString().padStart(7, "0");
  }
}


window.MyBotConnection = MyBotConnection;
window.BotLogger = BotLogger;
window.MyFakeBot = MyFakeBot;
window.HandoffBot = HandoffBot;
window.GenesysWidgetsBot = GenesysWidgetsBot;
window.ConnectionStatus = ConnectionStatus;